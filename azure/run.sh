#!/bin/bash

# Halt on error
set -e

for ARGUMENT in "$@"
do
    KEY=$(echo $ARGUMENT | cut -f1 -d=)
    VALUE=$(echo $ARGUMENT | cut -f2 -d=)
    
    export $KEY=$VALUE

    case "$KEY" in
        MAIN_INIT_URL)
			MAIN_INIT_URL=${VALUE}
			;;
        MAIN_INIT_USERNAME)
			MAIN_INIT_USERNAME=${VALUE}
			;;
        MAIN_INIT_PASSWORD)
			MAIN_INIT_PASSWORD=${VALUE}
			;;
    esac
done

if [ -z "$MAIN_INIT_URL" ]
then
    echo "Missing required argument: MAIN_INIT_URL"
    exit 1
fi

date
pwd

if [ -d "/custom-script/" ] 
then
	ls -lrth /custom-script
else
    echo "No folder /custom-script exists"
fi

if [ -n "$MAIN_INIT_USERNAME" ]
then
    echo "Fetching $MAIN_INIT_URL using curl, with separate username and password, and storing the result in the file mainInit.sh"
    curl -O -L --user $MAIN_INIT_USERNAME:$MAIN_INIT_PASSWORD "$MAIN_INIT_URL"
else
    echo "Fetching $MAIN_INIT_URL using wget, with inline username and password, and storing the result in the file mainInit.sh"
    wget --output-document mainInit.sh $MAIN_INIT_URL
fi

echo "Running script: bash mainInit.sh $@"
bash mainInit.sh "$@"